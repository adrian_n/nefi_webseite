<!DOCTYPE html>

<html lang="en">
<head>
  <meta name="generator" content=
  "HTML Tidy for HTML5 (experimental) for Mac OS X https://github.com/w3c/tidy-html5/tree/c63cc39">
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="viewport" content="width=device-width, initial-scale=1">

  <title>NEFI: Network Extraction From Images</title><!-- Bootstrap -->
  <link href="css/bootstrap.min.css" rel="stylesheet">
  <link href="css/sticky_footer.css" rel="stylesheet">
  <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
  <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
  <!--[if lt IE 9]>
      <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
      <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->

  <style>
  .big-figure {
      width:90%;
      padding:10px;
      display:block;
      margin:0 auto;
    }
  </style>
</head>

<body>
  <!-- Static navbar -->

  <nav class="navbar navbar-default navbar-static-top" role="navigation">
    <div class="container">
      <div class="navbar-header">
        <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target=
        "#navbar" aria-expanded="false" aria-controls="navbar"><span class="sr-only">Toggle
        navigation</span></button> <span class="navbar-brand"><a href="index.html">NEFI</a></span>
      </div>

      <div id="navbar" class="navbar-collapse collapse">
        <ul class="nav navbar-nav">
          <li class="active">
            <a href="gallery.html">Gallery</a>
          </li>

          <li class="inactive">
            <a href="guide.html">Guide</a>
          </li>

          <li class="active">
            <a href="download.html">Download</a>
          </li>

          <li class="active">
            <a href="credits.html">Acknowledgments</a>
          </li>
        </ul>

        <ul class="nav navbar-nav navbar-right">
          <li class="active">
            <a href="contact.html">Contact</a>
          </li>
        </ul>
      </div>
    </div>
  </nav>

  <div class="container">
    <!-- Main component for a primary marketing message or call to action -->

    <h1>Guide</h1>

    <h2>Content</h2>

    <ul>
      <li>
        <a href="#introduction">Introduction</a>
      </li>

      <li>
        <a href="#installation">Installation</a>
      </li>

      <li>
        <a href="#gettingstarted">Getting Started</a>

        <ul>
          <li>
            <a href="#physarum_start">Tutorial 1: Extracting the vein network of P.
            polycephalum</a>
          </li>

          <li>
            <a href="#dragonfly_start">Tutorial 2: Extracting the vein network of an insect
            wing</a>
          </li>

          <li>
            <a href="#commandline_start">Tutorial 3: Batch processing in command line mode</a>
          </li>
        </ul>
      </li>

      <li><a href="#the graph">Working with the Graph</a></li>

      <li>
        <a href="#limitations">Accuracy, performance and limitations</a>
      </li>

      <li>
        <a href="#details">Design philosophy, extendability and further reading</a>
      </li>

      <li>
        <a href="#troubleshooting">Troubleshooting and known issues</a>
      </li>

      <li>
        <a href="#mailinglist">Mailing List</a>
      </li>

      <li>
        <a href="#publication">Publication</a>
      </li>

      <li>
        <a href="#citing">How to cite NEFI</a>
      </li>
    </ul>

    <h2 id="introduction">Introduction</h2>

    <p>NEFI is a Python tool created to extract networks from images. Given a suitable 2D image of
    a network as input, NEFI outputs a mathematical representation of the structure of the depicted
    network as a weighted undirected planar <a href=
    "http://en.wikipedia.org/wiki/Graph_%28mathematics%29">graph</a>. Representing the structure of
    the network as a graph enables subsequent studies of its properties using tools and concepts
    from <a href="http://en.wikipedia.org/wiki/Graph_theory">graph theory</a>.</p>

    <p>NEFI was developed and tested primarily for the study of vein networks of the slime mold
    <a href="http://en.wikipedia.org/wiki/Physarum_polycephalum">P.&nbsp;polycephalum</a>. However, the
    authors successfully processed images of networks of different origin. Have a look at the examples in the
    <a href="/gallery.html">Gallery</a> for details.</p>

    <p>NEFI builds on top of well-documented open source libraries in order to provide a reliable,
    transparent and extendable collection of interchangeable solutions. NEFI facilitates single
    image analysis as well as batch processing and aims to enable scientists and practitioners of
    various domains to freely explore, analyze and process their data in an intuitive, hands-on
    fashion.</p>

    <p>Our major motivation in developing NEFI is to enable virtually everyone to automatically
    extract networks from images. While we may not be the first to attack this problem, we seek to
    overcome some of the limitations of existing solutions. To this end we present an extensible
    toolbox hidden behind an intuitive graphical user interface. NEFI was designed to give the
    practitioners in the labs the possibility to have a quick look at the pictures they just took
    to get a first impression of their observations. NEFI was designed to allow theoreticians to
    quickly falsify the predictions of their newest model using real world data. NEFI aims to
    enable and promote the application of graph theory across different fields and to be a useful
    tool for everyone seeking to bridge the gap between theory and experiment.</p>

    <h2 id="installation">Download and Installation</h2>

    <p>To download and install NEFI simply go to the <a href="download.html">Download</a> page and
    follow the instructions. While we are trying on to get our installers running on different
    platforms only the source code version will be publicly available. Please be careful to meet
    the listed requirements.</p>

    <h2 id="gettingstarted">Getting Started</h2>

    <p>So you managed to install NEFI successfully, great! The easiest way to actually run NEFI is
    to open a commandline window, navigate to NEFI's install directory and type:</p>
    <pre>
python NewGUI.py
</pre>

    <p>Provided all of NEFI's requirements are met, you should see NEFI's graphical user interface
    (GUI), ready to go.</p>

    <p>To get you started immediately, we begin with hands-on examples that introduce the most
    basic workflows of NEFI. Simultaneously the major functions, the graphical user interface and
    the resulting output are illustrated. By this point the reader who is primarily interested in
    using NEFI as a black-box collection of tools should be well-equipped to start processing his
    own data.</p>

    <figure style="max-width:50%; padding:10px; padding-left:20px; float:right">
      <img style="width:100%; max-width:419px; display:block; margin: 0 auto;" id="nefi_flowchart"
      src="images/nefi_flowchart.png" alt="Nefi's flowchart">

      <figcaption style="padding-bottom:1em;">
        Figure 1: A flow chart illustrating NEFI's pipeline components in green. Dashed arrows
        depict optional sections of the pipeline.
      </figcaption>
    </figure>

    <p>NEFI is a collection of image processing routines and graph algorithms designed to process
    2D digital images of various networks and network-like structures. Its main function is to
    execute a so-called extraction pipeline to analyze the structures depicted in the
    input image. An extraction pipeline, or short pipeline, denotes an ordered sequence of
    algorithms. The pipeline combines algorithms from up to four different classes: preprocessing,
    segmentation, graph detection and graph filtering, see <a href="#nefi_flowchart">Figure 1</a>.
    A typical pipeline may combine several preprocessing and graph filtering routines but must
    contain exactly one segmentation and exactly one graph detection routine. A successful
    execution will return a representation of the depicted structures in terms of a weighted
    undirected planar graph.</p>

    <p>NEFI's intended workflow and intuitive usage is centered around the concept of the pipeline.
    After selecting an input image, you have two options to proceed. You may either start from
    scratch and compose a new pipeline by mixing and matching algorithms from different classes or
    you may fall back on one of the predefined ready-to-go pipelines, make a couple of changes here
    and there and give it try. Both approaches offer considerable freedom and allow you to
    experiment with the available methods in order to close in on the optimal settings for
    your data. Once your pipeline works well, you can save it and use it in a
    different session or in conjunction with the commandline mode.</p>

    <h3 style="clear:both;" id="physarum_start">Tutorial 1: Extracting the vein network of P.&nbsp;
    polycephalum</h3>

    <p>In this example we demonstrate how to use NEFI's GUI, to
    construct a pipeline for processing a picture of the slime mold P.&nbsp;polycephalum. The image was
    produced by T.&nbsp;Mehlhorn in a collaboration with <a href=
    "http://www.frias.uni-freiburg.de/de/das-institut/archiv-frias/school-of-softmatter/fellows/manz">
    Prof. A. Manz</a>. Both gentlemen are members of the <a href=
    "http://www.kist-europe.de/index.php/en/">KIST Europe</a> in Saarbr&uuml;cken, Germany.</p>

    <h4>Step 1: Explore NEFI's GUI and meet the pipeline</h4>

    <p>After you have started NEFI, you will be presented with NEFI's GUI which was designed for
    intuitive no-nonsense usage. On the left-hand-side you should see the pipeline column
    containing a thumbnail of the default input picture that loads when NEFI starts, i.e. an image
    of our favorite slime mold P.&nbsp;polycephalum. Clicking any thumbnail will cause an enlarged
    version of it to appear in the center of NEFI's workspace, see <a href=
    "#nefi_gui_figure">Figure 2</a>. At the moment the pipeline consists only of the default input
    image. To process the image we can skip preprocessing which is optional and add a segmentation
    routine to the pipeline.</p>

    <figure class="big-figure">
      <img class="img-responsive" id="nefi_gui_figure" src="images/screenshots/nefi_gui_start.jpg"
      alt="">

      <figcaption>
        Figure 2: NEFI's GUI displaying the singular pipeline element on the left-hand-side and its
        enlargement on the center workspace. The menus on the right-hand-side can be used to add
        algorithms to the pipeline.
      </figcaption>
    </figure>

    <h4>Step 2: Add segmentation to the pipeline</h4>

    <p>To add a segmentation routine to the pipeline, select the image you want your routine to be
      applied to. In this case there is only the first image available. Now look at the menu on the
      right-hand-side. First select 'Segmentation' from the 'method' menu. The simplest way to segment an image into foreground and background is the application of a constant threshold: Every pixel that is darker than the threshold value becomes a foreground pixel, all other pixels become background pixels. While NEFI offers this simple method, choosing the right threshold value requires some experimentation. So instead we will use <a href= "http://en.wikipedia.org/wiki/Otsu%27s_method">'Otsu's Threshold'</a>. Using the 'algorithm' menu, select Otsu's threshold. Otsu's method calculates the optimal threshold value based on a clustering approach. This saves us the work of playing with the threshold parameter until we find a good value.</p><p>
      To execute the pipeline simply press the circle-shaped 'play' button in the top left corner of
      the GUI. After a few moments, the resulting segmented image is added to the pipeline and ready
      for visual inspection, see <a href= "#nefi_gui_segmented_figure">Figure 3</a>. Additional
      information such as pipeline progress, timings, etc. is available in the commandline window
      associated with NEFI. Next we need to use the segmented image to actually extract the depicted
      network from it. To do so we add the graph extraction algorithm to the end of the pipeline.</p>

    <figure class="big-figure">
      <img class="img-responsive" id="nefi_gui_segmented_figure" src=
      "images/screenshots/nefi_gui_segmented.png" alt="">

      <figcaption>
        Figure 3: NEFI automatically adds the results of the finished pipeline stages to the
        pipeline. In this case the segmented image has been added.
      </figcaption>
    </figure>

    <h4>Step 3: Add graph detection to the pipeline</h4>

    <p>To add a graph detection routine once more navigate to the thumbnail depicting the segmented
    image and click it. From the menu on the right-hand-side select 'Graph detection' and choose
    the 'Guo Hall graph detector'. Yes, it is currently the only option. The graph detection will
    first obtain a connected skeleton of the segmented image by using the Guo-Hall thinning method.
    Then the skeleton is examined in order to detect the nodes and the edges of the graph. During
    the process edge weights such as length and thickness are computed. To see the results execute
    the pipeline by pressing the 'play' button. Note that pipeline steps that have already been
    computed will not be computed again, provided there was no change to the settings. After the
    graph detection terminates you have two new images appended to the pipeline. The skeleton of
    the segmented image and the detected network superimposed on the original input, see <a href=
    "#nefi_gui_result_figure">Figure 4</a>. At this point it is advised to visually inspect the
    skeleton as well as the resulting graph carefully in oder check if the algorithm runs into
    difficulties somewhere.The quality of the detected graph depends largely on the interplay
    between segmentation and graph detection. If problems occur one can try to improve the results
    by experimenting with different segmentation methods in conjunction with some preprocessing.
    For more detailed advice see section <a href="#troubleshooting">Troubleshooting</a>.</p>

    <figure class="big-figure">
      <img class="img-responsive" id="nefi_gui_result_figure" src=
      "images/screenshots/nefi_gui_result.jpg" alt="">

      <figcaption>
        Figure 4: The result of the graph detection step is being displayed on the center
        workspace.
      </figcaption>
    </figure>

    <h4>Step 4: Add graph filtering to the pipeline</h4>

    <p>Upon closer inspection the user might notice that the detected network might not be
    connected everywhere although the original input network was. Typically smaller parts of the
    network that had only very weak connections to the rest disconnect as a result of the
    segmentation step. Another frequent cause for strong fragmentation is the use of very sensitive
    segmentation routines. Note that such artifacts are not necessarily a problem because they can
    often be dealt with through filtering.</p>

    <p>In order to clean up the graph and focus on the big connected part of the network one might
    be interested in filtering the small pieces. To do so we add another step to the pipeline.
    Navigate to the last thumbnail in the pipeline and click it. From the menu on the
    right-hand-side select 'Graph filtering' and 'Keep only largest connected component'. Pressing
    the 'play' button will add yet another image to the pipeline displaying the original image
    superimposed with the filtered graph, see <a href="#nefi_gui_result_filtered_figure">Figure
    5</a>. Additional filters can now be applied easily by repeating the steps of this
    subsection.</p>

    <figure class="big-figure">
      <img class="img-responsive" id="nefi_gui_result_filtered_figure" src=
      "images/screenshots/nefi_gui_result_filtered.jpg" alt="">

      <figcaption>
        Figure 5: The result after applying the filtering step is being displayed on the center
        workspace. It can be seen that the number of smaller parts that have been filtered is small
        as well (1631 components precisely).
      </figcaption>
    </figure>

    <h4>Step 5: Save the pipeline and selected images</h4>

    <p>At this point a working pipeline has been put together and the results of the included
    algorithms have been subject to visual inspection. The user could now try to improve the
    results by trying to replace parts of the pipeline or by adding additional routines. However,
    if the result is satisfactory as is, it can be saved to disk together with the whole pipeline.
    The saved pipeline can be reused in the next session or be applied from the command line.
    Pipelines saved to pipeline folder in NEFI's directory will appear listed in the pipeline menu
    after NEFI has restarted. To do so select 'Pipeline → Save pipeline' from the menu on the top
    left. In addition to that the image that is currently displayed on the workspace can be saved
    to disk using the save dialog also available via the menu on the top left. Unfortunately there
    is no dedicated 'Save graph' functionality at the moment. For now please refer to the 'tmp'
    directory in NEFI's directory. It contains all the results produced by the execution of the
    pipeline including the graphs. <em>Be careful, this directory is purged when you restart NEFI!.
    </em></p>

    <h3 id="dragonfly_start">Tutorial 2: Extracting the vein network of an insect wing</h3>

    <p>NEFI gives you considerable freedom in assembling your pipelines. However, sometimes it's
    simply more convenient to rely on a couple of sensible defaults. In this example we demonstrate
    how to use NEFI's predefined pipelines for processing a picture of a wing belonging to the
    dragonfly Ajax junius. The beautiful picture of a wing belonging to the common green darner,
    lat. Ajax junius, was provided generously by <a href="http://naturetime.wordpress.com/">Richard
    and Pam Winegar</a>.</p>

    <h4>Step 1: Load a user defined input image</h4>

    <p>After starting NEFI, use the load image button to open the image 'a_junius_wing.jpeg'
    residing in '/example_images' directory of NEFI's installation directory. The image should load
    to the first position in the pipeline, see <a href="#nefi_gui_start_dragonfly_figure">Figure
    6</a>. Select the image by clicking on it. Instead of assembling a pipeline by hand as in <a href=
    "#physarum_start">Tutorial&nbsp;1</a>, we use one of the predefined pipelines that
    were found to function well when NEFI was developed and tested.</p>

    <figure class="big-figure">
      <img class="img-responsive" id="nefi_gui_start_dragonfly_figure" src=
      "images/screenshots/nefi_gui_start_dragonfly.png" alt="">

      <figcaption>
        Figure 6: Picture of the wing of A.&nbsp;junius on the center workspace.
      </figcaption>
    </figure>

    <h4>Step 2: Select a predifined pipeline</h4>

    <p>To select a pipeline navigate to the 'Pipeline' menu entry on the top left and select 'A.
    junius'. To execute the pipeline press play. The computation may take a while (about 30 seconds
    on our machines) because the
    pipeline is rather involved and the input image size is very large with 5760 x 3840 pixel. The
    result should look like <a href="#nefi_gui_result_filtered_dragonfly_figure">Figure 7</a>. This
    pipeline is a good example of how clever filtering can really make the best of abundant
    segmentation information.</p>

    <figure class="big-figure">
      <img class="img-responsive" id="nefi_gui_result_filtered_dragonfly_figure" src=
      "images/screenshots/nefi_gui_result_filtered_dragonfly.png" alt="">

      <figcaption>
        Figure 7: The extracted vein network of the wing of A.&nbsp;junius on the center workspace.
      </figcaption>
    </figure>

    <h4>Step 3: Save selected images and the graph</h4>

    <p>As in the previous example the results of the pipeline execution are available now for
    visual inspection. Also any fine tuning to improve the results can easily start from here.
    Again, if desired any image produced in the process can be saved to disk. Also the extracted
    network is available for further analysis in the '\tmp' directory of NEFI. (A save button for the
    graph is coming soon!)</p>

    <h3 id="commandline_start">Tutorial 3: Batch processing in command line mode</h3>
        <p>
        Although visual inspection is important and a powerful tool to fine tune NEFI's algorithms, it becomes infeasible when it comes to processing a large set of input images. To deal with this case NEFI offers a command line mode that allows to apply a specified pipeline to every image in a given target folder. The user should construct a working pipeline and
        convince himself that it works satisfactory for a suitable small subset of the set of images that need to be processed. Once such a pipeline has been found it can saved to disk and applied using the following shell script:
        </p>

<pre>
#!/bin/bash
FILES=&lt;Directory&gt;
for f in $FILES
do
    python run.py -i ${image} -p &lt;Pipeline file&gt; -o &lt;Output directory&gt;
done 
</pre>
    <p>
    The NEFI command line mode can also be executed in parallel using <a href="http://www.gnu.org/software/parallel/">GNU Parallel </a>:
    </p>
<pre>
ls &lt;Directory&gt; | parallel python run.py -i ${1} -p &lt;Pipeline file&gt; -o &lt;Output directory&gt; :::
</pre>

    <h2 id="the graph">Working with the Graph</h2>
    <p>Once you tweaked the algorithms in your pipeline to extract an accurate graph from your input image, you probably want to work with the graph and perform some analysis. Embarassingly, NEFI currently doesn't have a button to save the graph, but you can get the graph in form of a <a href="https://networkx.github.io/documentation/networkx-1.9.1/reference/readwrite.multiline_adjlist.html?highlight=multiline#module-networkx.readwrite.multiline_adjlist">multiline adjacency list</a> from the 'tmp' directory of NEFI. Make sure to grab the file before restarting NEFI, since <em>this directory is purged on every restart.</em> Future versions of NEFI will allow you to save the graph in a variety of formats. Sorry for the temporary inconvenience.</p>
    <p>The graph format is very simple to parse. Lines that start with # are comments and can be ignored. The first non-comment line contains the coordinates of a node followed by its number of neighbors. The next lines contain the neighbors, followed by a dictionary of edge attributes. For example, it can begin like this:
<pre>
(956, 2654)|3
(958, 2652)|{'width': 2.0, 'length': 5.656856, 'pixels': 2, 'width_var': 0.0}
(957, 2652)|{'width': 2.0, 'length': 6.242642, 'pixels': 3, 'width_var': 0.0}
(956, 2652)|{'width': 2.0, 'length': 6.242642, 'pixels': 3, 'width_var': 0.0}
(644, 1672)|2
(643, 1672)|{'width': 3.3333333333333335, 'length': 5.828428000000001, 'pixels': 3, 'width_var': 0.88888888888888884}
(646, 1671)|{'width': 2.0, 'length': 4.828428000000001, 'pixels': 2, 'width_var': 0.0}
</pre>
The first node (956, 2654) has three neighbors. The next three lines name these neighbors, followed by the properties of the edges that connect them to (956, 2654). Then the next node follows, with two neighbors. Note that the order of the nodes or the values in the edge attribute dictionary is not deterministic and should not be relied on.</p>
<p>NEFI computes the width of an edge (and the variance of the width), as well as its length and the number of pixels that belong to this edge. Length and width are given in pixel units.
</p>
<p>Of course, if you use Python and the networkx library you don't have to write your own parser for the graph. Reading and analyzing the graph using the library is very simple. The only tricky part is parsing the tuple representing the node coordinates from a string back to a tuple of integers. You can use the following function to load a graph:
<pre>
import networkx as nx

def load_graph(path):
  from string import split
  def parse_node_type(string_literal):
    # Turns a string denoting a tuple of the form '(int, int)' to a tuple of the form
    # (int, int).
    braces_stripped = string_literal[1:-1]
    return tuple((int(x) for x in split(braces_stripped, ", ")))

  with open(path) as handle:
    return nx.read_multiline_adjlist(handle, nodetype = parse_node_type, delimiter = '|')
</pre></p><p>With the graph at hand we can analyze its properties. For example we can use the following code to print a very simple histogram of the edge-length distribution in the graph:
<pre>
import numpy
import math

def print_histogram(values, bins):
  #see
  #http://docs.scipy.org/doc/numpy/reference/generated/numpy.histogram.html
  hist, bins = numpy.histogram(values, bins, density=True)

  #print *'s proportional to the number of items in the bucket
  for b, num in zip(bins[1:],hist):
    print '<%(bin).2f\t' % {'bin':b}, '*'*int(num*250)

#see the snippet above
graph = load_graph('./adj_matrix.txt')


print "Number of nodes:", graph.number_of_nodes()
print "Number of edges:", graph.number_of_edges()
print "Edge length histogram:"
lengths = nx.get_edge_attributes(graph, 'length')
print_histogram([lengths[e] for e in graph.edges_iter()], 15)
</pre>
</p><p>For the <a href="examples/physarum.html">Physarum example</a> image, we can see that most of the edges are very short.
<pre>
$ python load_graph.py
Number of nodes: 147051
Number of edges: 189845
Edge length histogram:
<7.79   **************************************
<12.59  *********
<17.38  **
<22.18  *
<26.97
<31.76
<36.56
<41.35
<46.15
<50.94
<55.74
<60.53
<65.32
<70.12
<74.91
</pre>
You can download this example <a href="downloads/analysis_example.tar.bz2">from here</a>.
</p>

    <h2 id="limitations">Accuracy, performance and limitations</h2>

    <p>NEFI was designed for efficient and robust single image as well as batch processing.
    Depending on the characteristics of the input image and the selected pipeline the quality of
    the resulting graph may vary. The major factor determining the quality of the graph is the
    segmentation step.</p>

    <p>Many segmentation methods, including <a href=
    "http://en.wikipedia.org/wiki/Watershed_%28image_processing%29">Watershed</a> and <a href=
    "http://en.wikipedia.org/wiki/GrabCut">GrabCut</a> work very well if they do not have to deal
    with low-contrast pictures showing strong background color gradients. In such cases, more
    technically involved approaches are called for to overcome such challenging conditions.</p>

    <p>NEFI's accuracy is a measure of how closely the computed graph resembles the originally
    depicted input network. Thus, when using segmentation as necessary step in network extraction,
    NEFI inherits the strengths and the weaknesses of its algorithms. NEFI tends to produce a lot
    of artifacts and faulty segmentations if the input pictures show</p>

    <ul>
      <li>Highly irregular backgrounds with strong color/brightness gradients.</li>

      <li>Isolated low contrast regions.</li>

      <li>Networks with edges that are too fine to be segmented correctly with the given image
      resolution.</li>

      <li>Networks that are too dense too be separated correctly</li>
    </ul>

        <p>NEFI is designed to efficiently process large quantities of images. To this end NEFI
    outsources much of its computational load to highly optimized and reliable library code. The
    table below illustrates the effectiveness of a typical pipeline given input images of different
    size. The timings were obtained on a Mac notebook equipped with 2,4 GHz Intel i5 and 8 GB
    RAM.</p>

    <table class="table table-hover" style="width:100%">
      <thead>
        <tr>
          <th>Pipeline element</th>

          <th>Small image: 1152 x 864 pixels</th>

          <th>Large image: 5760 x 3840 pixels</th>
        </tr>
      </thead>

      <tbody>
        <tr>
          <td>Blur image</td>

          <td>&lt; 1 s</td>

          <td>&lt; 3 s</td>
        </tr>

        <tr>
          <td>Guided Watershed with adaptive threshold mask</td>

          <td>&lt; 1 s</td>

          <td>&lt; 8 s</td>
        </tr>

        <tr>
          <td>Guo-Hall Thinning</td>

          <td>&lt; 1 s</td>

          <td>&lt; 5 s</td>
        </tr>

        <tr>
          <td>Node detection</td>

          <td>&lt; 1 s</td>

          <td>&lt; 10 s</td>
        </tr>

        <tr>
          <td>Computing edge weights</td>

          <td>&lt; 2 s</td>

          <td>&lt; 10 s</td>
        </tr>

        <tr>
          <td>Compute largest connected component</td>

          <td>&lt; 2 s</td>

          <td>&lt; 4 s</td>
        </tr>
      </tbody>
    </table>

    <p>An additional limitation of NEFI consists of the inability to detect nodes of degree 2.
    NEFI's node detection relies on the skeleton of the segmented image. However, since the
    skeleton reflecting a path of nodes of degree two cannot be distinguished from a single long
    edge, degree 2 nodes are automatically contracted. The authors stress at this point that NEFI
    has its known limitations. NEFI it is not capable of dealing with every possible input picture
    and was never intended to do so. However, NEFI tries to make up for its weaknesses by offering
    the possibility to integrate additional algorithms tailored towards a challenging input.</p>

    <h2 id="details">Design philosophy, extendability and further reading</h2>

    <p>NEFI's approach is centered around the idea of intuitively combining algorithms to form an
    effective extraction pipeline. Thus NEFI does not stop at implementing a single solution to the
    network extraction problem but forms a flexible collection of powerful routines readily
    available for users of any experience level. We stress that NEFI does not require any
    prerequisite knowledge in image processing, computer vision or software design to be put to
    effective use. On the other hand, experts who are developing their own algorithms for network
    extraction are invited to consider using NEFI as an extendable framework. NEFI's modular design
    facilitates adding new methods to NEFI make it easier to compare different approaches against
    each other. Also, integrating your new algorithms into NEFI it might make it easier for you to
    reach wider audiences and thus gain a bigger impact.</p>

    <p>These straightforward considerations have become the guiding principles in NEFI's
    development and are reflected in NEFI's design and implementation choices.</p>

    <p>The language we choose to implement NEFI is Python. Python is flexible, easy to use and
    cross-platform. Above all, Python offers a large number of expertly written, well-documented
    and time-tested libraries. By heavily relying on library code NEFI becomes more efficient,
    better documented and thus easier to understand in detail. In addition to that NEFI becomes
    less error-prone and its maintainability is greatly increased. For more details on NEFI's
    dependencies see the <a href="download.html">Download</a> page.</p>

    <p>It is important to the developers of NEFI that the average user does not get caught up in
    unnecessary details when using NEFI. A tool should be only as complicated as it needs to be to
    do its job. However, there is a lot going on behind the scenes of every stage in the pipeline.
    For a detailed discussion of NEFI's design and its algorithms we refer the interested reader to
    our upcoming publication. It contains a full description of NEFI and serves as a more detailed
    companion document that goes side-by-side with this project page. For details, please see the
    section <a href="#publication">Publication</a>.</p>

    <h2 id="troubleshooting">Troubleshooting and known issues</h2>

    <p>THIS SECTION WILL BE COMING SOON.</p>

    <p>For the time being, please be aware of the fact that NEFI is currently in an early alpha
    stage with only three poor (PhD) students contributing to it. We are currently tracking many
    issues and are convinced that there are many more to be discovered. If you decided to use NEFI
    now please consider reporting any bugs, crashes or unexpected behavior to the <a href=
    "contact.html">developers</a>. To facilitate quick communication of reports we are working to
    enable everyone to open tickets in our project issue tracking system. Ultimately, we'd like to
    increase the number of developers and will be happy to get to know people who are willing to
    contribute.</p><!--         <h3 id="nothing">NEFI does not start</h3>
        <h3 id="starts">NEFI starts but ...</h3>
        <h3 id="executes">NEFI starts but ...</h3> -->

    <h2 id="mailinglist">Mailing List</h2>

    <p>NEFI is under active delevopment and updated on a regular basis. If you want to be notified
    about major changes and new versions, consider subscribing to our <a href=
    "contact.html">mailing list</a> hosted at the <a href=
    "http://www.mpi-inf.mpg.de/home/">Max-Planck Institute for Informatics</a>.</p>

    <h2 id="publication">Publication</h2>

    <p>NEFI is curerntly being peer reviewed. In the mean time you can find a preprint version of the full paper here&#58; <a href="http://arxiv.org/abs/1502.05241">NEFI&#58 Network Extraction Form Images</a> </p>

    <h2 id="citing">How to cite NEFI</h2>

    <p>If you use NEFI in your research, please add a citation. You can use the following BibTex
    entry in your paper.</p>
    <pre>
@Unpublished{Dirnberger2015,
     author    = "M. Dirnberger and A. Neumann and T. Kehl",
     title     = "{NEFI: Network Extraction From Images}",
     year      = "2015",
     archivePrefix = "arXiv",
     eprint        = "1502.05241",
     primaryClass  = "cs.CV"
}
</pre>
  </div><!-- /container -->

  <footer class="footer">
    <div class="container">
      <a href="http://www.mpi-inf.mpg.de/departments/algorithms-complexity/"><img height="50" src=
      "/images/mpilogo-inf-wide.png" alt="mpi logo"></a>

      <div style="float:right">
        <p class="small"><a href="http://www.mpi-inf.mpg.de/imprint/">Imprint</a></p>
      </div>
    </div>
  </footer><!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
  <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.1/jquery.min.js">
</script> <!-- Include all compiled plugins (below), or include individual files as needed -->
   <script src="js/bootstrap.min.js">
</script>
</body>
</html>
